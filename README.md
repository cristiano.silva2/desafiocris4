# Desafiocris4

**Desafio Mandic 4**
**Documentação Desafiocris4**

Instalação MariaDB:

Realizar a instalação DB MariaDB e configurar acesso 

- mysql -u root -p

Verificar status DB;

- sudo systemctl status mariadb

https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-18-04-pt


**Instalar Redis no Ubuntu Usando o Comando APT** 

- sudo apt install redis

**Instalar Flask**

- pip install flask

**Instalar Mysql Connector**

- mysql-connector-python

**Instalar Boto3**

- pip install boto3

**Criar um arquivo chamado Dockerfile**

`FROM python:2
COPY . /app
RUN pip install flask && pip install mysql-connector-python && pip install redis && pip install flask-redis && pip install flask-mysql && pip install boto3
WORKDIR /app
CMD python app.py
EXPOSE 5000`

**Criar um SQS na AWS para conexão do Banco**

``host=xx.xxx.xxx
[11:00]
database=desafio
[11:00]
user=desafio
[11:01]
password=xxxx``

**Criar um arqruivo app.py no VSCODE**

````from flask import Flask, redirect, render_template, request, url_for, jsonify
import mysql.connector, os
from mysql.connector import Error
from flaskext.mysql import MySQL
import redis, boto3````

````app = Flask(__name__)
@app.route('/healthcheck')
def healthcheck():
    api = versaoAPI()
    sts = status()
    mysql = conexaodb()
    nosql = NOSQL()
    fila = FILA()
    return jsonify(api, sts, mysql, nosql, fila)

def versaoAPI():
    versao = 'API: 1.0'
    return (versao)

def status():
    statusDeAcesso = 'Status 200'
    return(statusDeAcesso)
    
def conexaodb():
    conn = None
    try:
        conn = mysql.connector.connect(host='52.204.4.116',
           user='desafio04',
           password='passwd',
           db='desafio04'
         )
        if conn.is_connected():
            return ('Mysql OK')
    except:
        return ('Mysql: Erro')

def FILA():
    conectfila = boto3.client('sqs', aws_access_key_id=os.getenv('access'),aws_secret_access_key=os.getenv('secret'), region_name=('us-east-1')) 
    geturl = conectfila.get_queue_url(QueueName='filacris')

    return('Fila: OK')


def NOSQL():
    conn =  None
    try:
        conn = redis.StrictRedis(host='52.204.4.116', port=6379, password='') 
        if conn.ping() == True:
            return ('Nosql: OK')
    except: 
        return('Nosql: ERRO')


if __name__ == '__main__':
        app.run(host='0.0.0.0', port='5000', debug=True)````
``````````

**Comando para Commit Imagem Docker:**
- docker commit 70e861e16684imagem_id cristianofeliciano/crisdesafio4

**Comando para Push imagem:**
- docker push imagem_id cristianofeliciano/crisdesafio4

**Exportar as Credenciais AWS**
export AWS_SECRET_ACCESS_KEY="xxxxx"
export AWS_ACCESS_KEY_ID="xxxxx"

**Comando para executar Docker imagem**:
- docker run -it -p 5000:5000 -e access=xxxxx -e secret=xxxxx crisdesafio4:1.0

**Verificar se API está ok:**
- http://0.0.0.0:5000/healthcheck


**Referencias;**
Instalação MariaDB: https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-18-04-pt

Docker: https://docs.docker.com/

Docker na MUC: https://muc.mandic.com.br/course/view.php?id=216

Docker na MUC: https://muc.mandic.com.br/course/view.php?id=47

Play with Docker: https://labs.play-with-docker.com/

Flask: https://flask-ptbr.readthedocs.io/en/latest/

Flask: https://developersoapbox.com/basic-rest-api-using-flask-and-mysql-get/

Docker: https://qastack.com.br/programming/30494050/how-do-i-pass-environment-variables-to-docker-containers

Docker: https://www.youtube.com/watch?v=SyvU4fOVpp0

Criando uma API REST com Flask: https://www.youtube.com/watch?v=N6cZ6aHvLYs
